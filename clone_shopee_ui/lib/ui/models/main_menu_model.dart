import 'package:flutter/material.dart';

class MainMenuModel {
  late String title;
  late Color color;
  late String image;

  MainMenuModel({required this.title, required this.color, required this.image});

}