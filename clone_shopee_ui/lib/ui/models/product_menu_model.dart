import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class ProductMenu extends Equatable {
  final String name;
  final String category;
  final String imageUrl;
  final double price;
  final bool isTopSearch;
  final bool isPopular;

  const ProductMenu({
    required this.name,
    required this.category,
    required this.imageUrl,
    required this.price,
    required this.isTopSearch,
    required this.isPopular,
  });

  @override
  List<Object?> get props => [
        name,
        category,
        imageUrl,
        price,
        isTopSearch,
        isPopular,
      ];

  static List<ProductMenu> productMenu = [
    ProductMenu(
      name: "Kính cường lực KingKong",
      category: "Screen Protector",
      imageUrl: "https://salt.tikicdn.com/ts/product/29/1f/ec/718b1685ddc9756ba2469908797b6ba0.jpg",
      price: 69000,
      isTopSearch: true,
      isPopular: false,
    ),

    ProductMenu(
      name: "iPad Pro 2021",
      category: "Smart Phone",
      imageUrl: "https://vcdn-sohoa.vnecdn.net/2020/05/13/ipad-pro-2020-high-7992-158935-6594-9100-1589354947.jpg",
      price: 21000000,
      isTopSearch: false,
      isPopular: true,
    ),

    ProductMenu(
      name: "iPhone 13 Pro Max",
      category: "Smart Phone",
      imageUrl: "https://cdn.tgdd.vn/Products/Images/42/230521/iphone-13-pro-sierra-blue-600x600.jpg",
      price: 33000000,
      isTopSearch: true,
      isPopular: true,
    ),

    ProductMenu(
      name: "Cốc Sạc Anker",
      category: "Cable",
      imageUrl: "https://cdn.tgdd.vn/Products/Images/9499/230266/230266-600x600.jpg",
      price: 499000,
      isTopSearch: true,
      isPopular: false,
    ),

    ProductMenu(
      name: "Loa JBL Go 2",
      category: "Soundcore",
      imageUrl: "https://didongviet.vn/pub/media/catalog/product//l/o/loa-bluetooth-jbl-go-2-didongviet_3.jpg",
      price: 299000,
      isTopSearch: false,
      isPopular: true,
    ),

    ProductMenu(
      name: "Yến mạch",
      category: "Food",
      imageUrl: "https://xuanan.com.vn/wp-content/uploads/2021/08/yenmach_anlien800-sua.jpg",
      price: 120000,
      isTopSearch: true,
      isPopular: false,
    ),

    ProductMenu(
      name: "Áo Khoác Nữ",
      category: "Shirt",
      imageUrl: "https://cf.shopee.vn/file/90333d4f86b9ef3b6e43dd5e81e7fdd7",
      price: 199000,
      isTopSearch: true,
      isPopular: true,
    ),

    ProductMenu(
      name: "Dầu Gội Đầu",
      category: "Hairs Soap",
      imageUrl: "https://thuvienmuasam.com/uploads/default/original/2X/a/a1853c6ce19fe77c9b1f0ca6de99f76b70963e03.jpeg",
      price: 99000,
      isTopSearch: false,
      isPopular: true,
    ),

    ProductMenu(
      name: "Máy Lạnh LG",
      category: "Furniture",
      imageUrl: "https://cdn.tgdd.vn/Products/Images/2002/234170/Slider/lg-v10api1-160621-0150370.jpg",
      price: 17000000,
      isTopSearch: true,
      isPopular: false,
    ),

    ProductMenu(
      name: "Bàn Học Bằng Gỗ",
      category: "Furniture",
      imageUrl: "https://bizweb.dktcdn.net/thumb/grande/100/361/907/products/ban-hoc-co-gia-sach-14.jpg",
      price: 20000000,
      isTopSearch: false,
      isPopular: true,
    ),
  ];
}
