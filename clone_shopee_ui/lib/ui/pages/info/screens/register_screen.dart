import 'package:clone_shopee_ui/ui/pages/info/services/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextEditingController emailController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();

    final authService = Provider.of<AuthService>(context);
    var obscureText = true;

    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: emailController,
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.email,
                  color: Colors.black,
                ),
                labelText: "Email",
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: passwordController,
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.lock,
                  color: Colors.black,
                ),
                suffixIcon: GestureDetector(
                  onTap: () {
                    obscureText = !obscureText;
                  },
                  child: obscureText
                      ? const Icon(
                    Icons.visibility_off,
                    color: Colors.black12,
                  )
                      : Icon(
                    Icons.visibility,
                    color: Colors.black,
                  ),
                ),
                labelText: "Password",
              ),
            ),
          ),
          ElevatedButton(
            onPressed: () async {
                await authService.createUserWithEmailAndPassword(
                emailController.text,
                passwordController.text,
              );
              Navigator.pushNamed(context, '/login');
            },
            child: Text('Register'),
          ),
        ],
      ),
    );
  }
}
