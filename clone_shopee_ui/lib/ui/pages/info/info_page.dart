import 'package:clone_shopee_ui/ui/pages/info/screens/login_screen.dart';
import 'package:clone_shopee_ui/ui/pages/info/screens/register_screen.dart';
import 'package:clone_shopee_ui/ui/pages/info/services/auth_service.dart';
import 'package:clone_shopee_ui/ui/pages/info/wrapper.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// class InfoPage extends StatefulWidget {
//   const InfoPage({Key? key}) : super(key: key);
//
//   @override
//   _InfoPageState createState() => _InfoPageState();
// }
//
// class _InfoPageState extends State<InfoPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Center(
//         child: Text("Trang Cá Nhân", style: TextStyle(fontSize: 30),)
//     );
//   }
// }


// MaterialApp(
//
// initialRoute: '/',
// routes: {
// '/': (context) => HomeScreen();
// '/login': (context) => LoginScreen();
// '/register': (context) => RegisterScreen();
// },
// );


class InfoPage extends StatelessWidget {
  const InfoPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<AuthService>(
          create: (_)  => AuthService(),
        ),

      ],
      child: MaterialApp(
        title: 'Flutter Auth',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
          initialRoute: '/',
          routes: {
            '/': (context) => Wrapper(),
            '/login': (context) => LoginScreen(),
            '/register': (context) => RegisterScreen(),
        },


      ),
    );
  }
}

