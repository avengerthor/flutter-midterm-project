import 'package:clone_shopee_ui/ui/models/product_menu_model.dart';
import 'package:clone_shopee_ui/ui/pages/home/widgets/product_card.dart';
import 'package:clone_shopee_ui/ui/pages/home/widgets/product_menu_carousel.dart';
import 'package:clone_shopee_ui/ui/pages/home/widgets/section_title.dart';
import 'package:flutter/material.dart';

class SecondMenu extends StatelessWidget {
  const SecondMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(),
        SectionTile(title: "TÌM KIẾM HÀNG ĐẦU"),
        // Product Card view
        // ProductCard(
        //   productMenu: ProductMenu.productMenu[0],
        // ),
        //Product Carousel
        ProductMenuCarousel(
          products: ProductMenu.productMenu
              .where((productMenu) => productMenu.isTopSearch)
              .toList(),
        ),
        SectionTile(title: "TÌM KIẾM PHỔ BIẾN"),
        ProductMenuCarousel(
          products: ProductMenu.productMenu
              .where((productMenu) => productMenu.isPopular)
              .toList(),
        ),
      ],
    );
  }
}
