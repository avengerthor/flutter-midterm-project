import 'package:flutter/material.dart';

class DevideFrame extends StatelessWidget {
  const DevideFrame({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 15,
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: Colors.grey.shade200,
        ),
      ),
    );
  }
}
