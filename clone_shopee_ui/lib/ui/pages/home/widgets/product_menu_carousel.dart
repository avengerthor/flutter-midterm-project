import 'package:clone_shopee_ui/ui/pages/home/widgets/product_card.dart';
import 'package:flutter/material.dart';

import '../../../models/product_menu_model.dart';

class ProductMenuCarousel extends StatelessWidget {
  final List<ProductMenu> products;
  const ProductMenuCarousel({
    Key? key,
    required this.products,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: SizedBox(
        height: 165,
        child: ListView.builder(
            shrinkWrap: true,
            padding: const EdgeInsets.symmetric(
              horizontal: 10.0,
              vertical: 10.0,
            ),
            scrollDirection: Axis.horizontal,
            itemCount: products.length,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.only(right: 5.0),
                child:
                ProductCard(productMenu: products[index],),
              );
            }),
      ),
    );
  }
}