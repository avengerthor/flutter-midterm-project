import 'package:clone_shopee_ui/ui/pages/info/screens/login_screen.dart';
import 'package:clone_shopee_ui/ui/pages/info/screens/register_screen.dart';
import 'package:clone_shopee_ui/ui/pages/info/wrapper.dart';
import 'package:flutter/material.dart';

import '../../feeds/shopee_feed_page.dart';
import '../../info/info_page.dart';
import '../../live/live_page.dart';
import '../../notification/notification_page.dart';
import '../home_page.dart';

class BottomNavPage extends StatefulWidget {
  const BottomNavPage({Key? key}) : super(key: key);

  @override
  _BottomNavPageState createState() => _BottomNavPageState();
}

class _BottomNavPageState extends State<BottomNavPage> {

  int _currentIndex = 0;
  final tabs = [
  HomePage(),
  FeedPage(),
  LivePage(),
  NotifPage(),
  InfoPage(),];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: IndexedStack(
          index: _currentIndex,
          children: tabs,
        ),
        bottomNavigationBar: BottomNavigationBar(
          // fixedColor: Colors.deepOrange,
          type: BottomNavigationBarType.fixed,
          showUnselectedLabels: false,
          unselectedItemColor: Colors.grey,
          selectedItemColor: Colors.deepOrange,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Home",
              // backgroundColor: Colors.red,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.dynamic_feed_outlined),
              label: "Shopee Feed",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.video_camera_back_sharp),
              label: "Live",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications_active),
              label: "Thông báo",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: "Tôi",
              backgroundColor: Colors.red,
            ),
          ],
          currentIndex: _currentIndex,
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
        ),
      ),
    );
  }
}
