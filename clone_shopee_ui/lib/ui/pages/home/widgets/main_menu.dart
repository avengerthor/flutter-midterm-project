import 'package:clone_shopee_ui/ui/viewmodels/main_menu_viewmodel.dart';
import 'package:flutter/material.dart';

import '../../../models/main_menu_model.dart';

class MainMenu extends StatelessWidget {

  final List<MainMenuModel> _menus = MainMenuViewModel().getMainMenu();

  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: EdgeInsets.only(top: 30),
      height: 220,
      padding: EdgeInsets.only(top: 18, bottom: 1),
      child: GridView.builder(
        padding: EdgeInsets.symmetric(horizontal: 12),
        scrollDirection: Axis.horizontal,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 1.0,
        ),
        itemBuilder: (context, index) {
          final MainMenuModel menu = _menus[index];
          return Column(
            children: [
              Container(
                // color: index % 2 == 0 ? Colors.amber : Colors.red,
                width: 45,
                height: 45,
                // margin: EdgeInsets.only(bottom: 8),

                child: TextButton(
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(14),
                      side: BorderSide(
                        color: Colors.black12,
                      ),
                    ),
                    padding: EdgeInsets.all(8),
                    primary: Colors.white,
                  ),
                  onPressed: () {
                    //todo
                  },
                  child: Image.network(
                    menu.image,
                    // "https://cdn1.iconfinder.com/data/icons/common-version-3-0/1024/Map-256.png",
                    color: menu.color,
                  ),
                ),
              ),
              SizedBox(height: 4),
              Text(
                menu.title,
                maxLines: 2,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 11),
              ),
              // SizedBox(height: 4),
            ],
          );
        },
        itemCount: _menus.length,
      ),
    );
  }
}
