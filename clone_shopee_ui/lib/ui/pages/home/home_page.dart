import 'package:clone_shopee_ui/ui/pages/feeds/shopee_feed_page.dart';
import 'package:clone_shopee_ui/ui/pages/home/widgets/banner_slider.dart';
import 'package:clone_shopee_ui/ui/pages/home/widgets/divide_frame.dart';
import 'package:clone_shopee_ui/ui/pages/home/widgets/main_menu.dart';
import 'package:clone_shopee_ui/ui/pages/home/widgets/second_menu.dart';
import 'package:clone_shopee_ui/ui/pages/info/info_page.dart';
import 'package:clone_shopee_ui/ui/pages/live/live_page.dart';
import 'package:clone_shopee_ui/ui/pages/notification/notification_page.dart';

import 'widgets/header.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _scrollController = TrackingScrollController();

  // int _currentIndex = 0;
  final tabs = [
    HomePage(),
    FeedPage(),
    LivePage(),
    NotifPage(),
    InfoPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(children: [
          SingleChildScrollView(
            controller: _scrollController,
            child: Column(
              children: [
                BannerSlider(),
                MainMenu(),
                DevideFrame(),
                SecondMenu(),
                Container(
                  margin: EdgeInsets.only(top: 100),
                  // color: Colors.red,
                  // child: Text("Update in next version"),
                  // height: 1500,
                  // width: double.infinity,
                ),
              ],
            ),
          ),
          Header(_scrollController),
        ]),
      ),
    );
  }
}
