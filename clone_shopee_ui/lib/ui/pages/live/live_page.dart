import 'package:flutter/material.dart';

class LivePage extends StatefulWidget {
  const LivePage({Key? key}) : super(key: key);

  @override
  _LivePageState createState() => _LivePageState();
}

class _LivePageState extends State<LivePage> {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text("Livestream", style: TextStyle(fontSize: 30),)
    );
  }
}