import 'package:flutter/material.dart';
import '../models/main_menu_model.dart';

class MainMenuViewModel {
  List<MainMenuModel> getMainMenu() {
    return [
      MainMenuModel(
        image: "https://cdn4.iconfinder.com/data/icons/unigrid-baby/59/012_fork_spoon_baby-512.png",
        title: "ShopeeFood - Giảm 155K",
        color: Colors.deepOrange,
      ),
      MainMenuModel(
        image: "https://cdn3.iconfinder.com/data/icons/black-friday-5-cut-line/468/35-cheap-512.png",
        title: "Gì Cũng Rẻ - Mua Là Freeship",
        color: Colors.deepOrangeAccent,
      ),
      MainMenuModel(
        image: "https://cdn3.iconfinder.com/data/icons/shopping-511/960/Flash_sale_Offer_Promotion_Discount_Special_Marketing_Shopping_cart-512.png",
        title: "Khung Giờ Săn Sale",
        color: Colors.amber,
      ),
      MainMenuModel(
        image: "https://cdn3.iconfinder.com/data/icons/major-currencies-24px-glyphs/48/dong_vietnamese_business_VND-512.png",
        title: "Mã Giảm Giá",
        color: Colors.amber,
      ),
      MainMenuModel(
        image: "https://cdn4.iconfinder.com/data/icons/business-finance-1-1/128/shipping-free-512.png",
        title: "Miễn Phí Vận Chuyển",
        color: Colors.green,
      ),
      MainMenuModel(
        image: "https://cdn2.iconfinder.com/data/icons/smartphone-services-1/85/mobile_phone_smartphone_cellphone_vibrate_vibration_ring-512.png",
        title: "Nạp Thẻ & Dịch Vụ",
        color: Colors.green,
      ),
      MainMenuModel(
        image: "https://cdn3.iconfinder.com/data/icons/e-commerce-and-shopping-glyph-3/64/04-Cashback-512.png",
        title: "Hoàn Xu Lên Đến 300K",
        color: Colors.deepOrange,
      ),
      MainMenuModel(
        image: "https://cdn4.iconfinder.com/data/icons/crowdfunding-line/64/box_gift_coin_money_investment-512.png",
        title: "Săn Thưởng Shopee",
        color: Colors.blueAccent,
      ),
      MainMenuModel(
        image: "https://cdn3.iconfinder.com/data/icons/black-solid-labels-2/128/50_sale_label_offer_percent-512.png",
        title: "Hàng Hiệu -50% - Deal 50%",
        color: Colors.red,
      ),
      MainMenuModel(
        image: "https://cdn0.iconfinder.com/data/icons/smoothies-vector-icons-volume-5/48/285-512.png",
        title: "Hàng Quốc Tế",
        color: Colors.greenAccent,
      ),MainMenuModel(
        image: "https://cdn2.iconfinder.com/data/icons/online-shope/25/bag-512.png",
        title: "Shopee Mall",
        color: Colors.red,
      ),MainMenuModel(
        image: "https://cdn1.iconfinder.com/data/icons/financial-45/23/coin-512.png",
        title: "Săn Xu Mỗi Ngày",
        color: Colors.amber,
      ),
      MainMenuModel(
        image: "https://cdn1.iconfinder.com/data/icons/personal-development-glyph-2/64/objectives-man-person-star-development-512.png",
        title: "Ưu Đãi Thành Viên - Tới 50%",
        color: Colors.deepOrange,
      ),
      MainMenuModel(
        image: "https://cdn3.iconfinder.com/data/icons/font-awesome-solid/576/money-bill-1-wave-1024.png",
        title: "Deal Sốc Từ 1K",
        color: Colors.amber,
      ),
      MainMenuModel(
        image: "https://cdn0.iconfinder.com/data/icons/business-finance-1-1/128/handshake3-512.png",
        title: "Ưu Đãi Đối Tác",
        color: Colors.blueAccent,
      ),
      MainMenuModel(
        image: "https://cdn2.iconfinder.com/data/icons/sport-136/128/1_billiard_ball_ball_championship_competition_play_snooker-512.png",
        title: "Chọn 6 Số Trúng Tiền Triệu",
        color: Colors.deepOrangeAccent,
      ),
      MainMenuModel(
        image: "https://cdn2.iconfinder.com/data/icons/network-and-communications-24/512/76_Map_Pin_Location_Ticket_Cinema-512.png",
        title: "ShopeePay Gần Bạn - Chỉ Từ 1K",
        color: Colors.deepOrange,
      ),
      MainMenuModel(
        image: "https://cdn0.iconfinder.com/data/icons/stock-market-and-finance/29/stock-market-financial-1005-512.png",
        title: "Shopee Premium",
        color: Colors.amber,
      ),
      MainMenuModel(
        image: "https://cdn0.iconfinder.com/data/icons/e-commerce-135/48/74_shopping-basket-buy-shope-commerce-512.png",
        title: "Shopee Mart - Siêu Thị Cuối Tuần",
        color: Colors.blue,
      ),
      MainMenuModel(
        image: "https://cdn2.iconfinder.com/data/icons/user-and-group/64/User_And_Group-11-512.png",
        title: "Giới Thiệu Bạn Nhận Ngay Xu",
        color: Colors.redAccent,
      ),
      MainMenuModel(
        image: "https://cdn1.iconfinder.com/data/icons/seo-internet-marketing-set-2/33/responsive_design-512.png",
        title: "Tech Zone - Voucher Đến 1TR",
        color: Colors.blueAccent,
      ),
    ];
  }
}
